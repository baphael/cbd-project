"""
Purpose

Shows how to use the AWS SDK for Python (Boto3) with the Amazon Elastic Compute Cloud
(Amazon EC2) API to terminate all instances and clean up additional resources.
"""

import utils
from botocore.exceptions import ClientError

def delete_key_pair(key_name):
    """
    Deletes a key pair and the specified private key file.
    :param key_name: The name of the key pair to delete.
    """
    try:
        utils.ec2_resource.KeyPair(key_name).delete()
        print(f"Deleted Key {key_name}.")
    except ClientError:
        print(f"Couldn't delete key {key_name}.")
        raise


def delete_security_group(group_id):
    """
    Deletes a security group.
    :param group_id: The ID of the security group to delete.
    """
    try:
        utils.ec2_resource.SecurityGroup(group_id).delete()
        print(f"Deleted security group {group_id}.")
    except ClientError:
        print(f"Couldn't delete security group {group_id}.")
        raise

def delete_vpc(vpc_id):
    """
    Deletes a VPC.
    :param vpc_id: The ID of the VPC to delete.
    """
    try:
        vpc = utils.ec2_resource.Vpc(vpc_id)
         # detach and delete all gateways associated with the vpc
        for gw in vpc.internet_gateways.all():
            vpc.detach_internet_gateway(InternetGatewayId=gw.id)
            gw.delete()
        # delete all route table associations
        for rt in vpc.route_tables.all():
            for rta in rt.associations:
                if not rta.main:
                    rta.delete()
                    rt.delete()
        # delete subnets
        for subnet in vpc.subnets.all():
            subnet.delete()
        vpc.delete()
        print(f"Deleted VPC {vpc_id}.")
    except ClientError:
        print(f"Couldn't delete VPC {vpc_id}.")
        raise

def terminate_instance(instance_id):
    """
    Terminates an instance. The request returns immediately. To wait for the
    instance to terminate, use Instance.wait_until_terminated().
    :param instance_id: The ID of the instance to terminate.
    """
    try:
        utils.ec2_resource.Instance(instance_id).terminate()
        print(f"Terminating instance {instance_id}.")
    except ClientError:
        print(f"Couldn't terminate instance {instance_id}.")
        raise

def teardown(vm_instances, security_groups, key_pairs, vpcs):
    """
    Cleans up all resources created by the script, including terminating instances.
    After an instance is terminated, it persists in the list
    of instances in your account for up to an hour before it is ultimately removed.
    :param vm_instances: The instances to terminate.
    :param security_groups: The security groups to delete.
    :param key_pair: The security key pair to delete.
    :param key_file_name: The private key file to delete.
    :param vpcs: The VPCs to delete.
    """
    for instance in vm_instances:
        terminate_instance(instance.id)
    
    terminating_waiter = utils.ec2_client.get_waiter("instance_terminated")
    terminating_waiter.wait(InstanceIds=[i.instance_id for i in vm_instances])
    print("Terminated all the instances.")

    for security_group in security_groups['SecurityGroups']:
        delete_security_group(security_group['GroupId'])
    print("Deleted security groups.")

    for key_pair in key_pairs:
        delete_key_pair(key_pair['KeyName'])
    print("Deleted keys.")

    for vpc in vpcs:
        delete_vpc(vpc['VpcId'])
    print("Deleted VPCs.")


if __name__ == '__main__':

    print("terminating all instances and cleaning up additional resources.")

    # Fetch and extract instances from cbdproject
    running_instances = utils.get_running_instances()

    # Fetch security groups of cbdproject
    security_groups = utils.get_security_groups()

    # Fetch key pairs of cbdproject
    key_pairs = utils.get_key_pairs()

    # Fetch VPCs of cbdproject
    vpcs = utils.get_vpcs()

    teardown(running_instances, security_groups, key_pairs, vpcs)
    print("Cluster cbdproject deleted successfuly.")