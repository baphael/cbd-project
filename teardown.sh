#!/bin/bash

#Go to virtual environment
echo -n "Go to CBD python3 virtual environment... "
source ~/.cbdproject/bin/activate
if [[ $? > 0 ]]; then
    echo "Warning : Could not go to virtual environment !"
else
    echo "OK"
fi

#Destroy AWS cluster
python ec2_teardown.py

#Destroy virtual environment
deactivate
rm -rf ~/.cbdproject