"""
Purpose

Use the AWS SDK for Python (Boto3) with the Amazon Elastic Compute Cloud
(Amazon EC2) API to deploy a Kubenetes cluster.
"""

import utils
from fabric import Connection

# The command that will be used for the workers to join the cluster
kube_join_cmd = ""

def kube_install(nodeClient, master, slaves):
    '''
    Install the common part of all nodes for Kubernetes.
    :param nodeClient: The SSH client of the node.
    :param master: master instance.
    :param slaves: slave instances.
    '''
    nodeClient.run("mkdir -p $HOME/.kube", hide='stdout')
    nodeClient.sudo("apt-get update -y", hide='stdout')
    nodeClient.sudo("swapoff -a")
    print("\tInstalling Docker ...", end='', flush=True)
    nodeClient.sudo("wget -qO- https://get.docker.com/ | sh", hide='both')
    nodeClient.sudo("usermod -aG docker ubuntu")
    print("Done")
    print("\tConfiguring node ...", end='', flush=True)
    nodeClient.sudo("echo net.bridge.bridge-nf-call-ip6tables = 1 " +
                    "| sudo tee /etc/sysctl.d/k8s.conf", hide='stdout')
    nodeClient.sudo("echo net.bridge.bridge-nf-call-iptables = 1 " +
                    "| sudo tee -a /etc/sysctl.d/k8s.conf", hide='stdout')
    nodeClient.sudo("sysctl --system", hide='both')
    nodeClient.sudo("curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg " +
                    "| sudo apt-key add -", hide='both')
    nodeClient.sudo("echo deb https://apt.kubernetes.io/ kubernetes-xenial main " +
                    "| sudo tee /etc/apt/sources.list.d/kubernetes.list", hide='stdout')
    nodeClient.sudo("apt-get update -y", hide='stdout')
    print("Done")
    print("\tInstalling packages ...", end='', flush=True)
    nodeClient.sudo("apt-get -y install kubelet kubeadm kubectl", hide='both')
    print("Done")

def configure_master(masterClient, master, slaves, subnet):
    """
    configure the master to install Kubernetes.
    :param masterClient: the SSH client of the master.
    :param master: master instance.
    :param slaves: slave instances.
    :param subnet: the private subnet to associate with Kubernetes.
    """
    print("Configuring master node")

    kube_install(masterClient, master, slaves)

    print(f"\tCreating pod network {subnet} on master...", end='', flush=True)
    result = masterClient.sudo(f"kubeadm init --pod-network-cidr={subnet} " +
        "--ignore-preflight-errors='all'", hide='both')
    print("Done")

    # Parse the result and extract the join command for slave nodes
    parsed_result = result.stdout.splitlines()
    for i in range(len(parsed_result)):
        if parsed_result[i].startswith("kubeadm join "):
            global kube_join_cmd
            kube_join_cmd = parsed_result[i] + '\n' + parsed_result[i+1]
            print("\tTo manually join the Kubernetes cluster, please run the following command:")
            print(kube_join_cmd + '\n')

    masterClient.sudo("cp /etc/kubernetes/admin.conf $HOME/.kube/config")
    masterClient.sudo("chown $(id -u):$(id -g) $HOME/.kube/config")
    print("\tDeploying flannel network to the kubernetes cluster...", end='', flush=True)
    masterClient.run("kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/" +
        "master/Documentation/kube-flannel.yml", hide='both')
    print("Done")

    print("Copying ~/.kube/config from master to slaves...")
    for slave in slaves:
        masterClient.run(f"scp -i ~/.ssh/cluster-key " +
            f"-oStrictHostKeyChecking=accept-new ~/.kube/config ubuntu@{ slave.private_ip_address }:~/")
    print("Done")
 
def configure_slave(slaveClient, master, slaves, hostname):
    '''
    Configure a slave node to handle Kubernetes deployement.
    :param slaveClient: the SSH client of the slave. 
    :param master: master instance.
    :param slaves: slave instances.
    :param hostname: the slave's hostname.
    '''
    print(f"Configuring slave node { hostname }")

    kube_install(slaveClient, master, slaves)

    slaveClient.sudo("mv ~/config ~/.kube")

    print("\tJoining cluster...", end='', flush=True)
    slaveClient.sudo(f"{kube_join_cmd} --skip-phases=preflight", hide='both')
    print("Done")
    print("\tSlave node successfully joined cluster")

def deploy(vm_instances, subnet):
    """
    deploys the Kubernetes cluster on the specified instances.
    :param instances: The instances of the cluster.
    :param subnet: The private subnet of the cluster.
    """
    slaveClients = {}
    master = utils.get_master(vm_instances)
    slaves = utils.get_slaves(vm_instances)

    # Create SSH session for the master
    masterClient = utils.connect_ssh(master.public_ip_address)

    configure_master(masterClient, master, slaves, subnet)

    # Configure each slave
    for i in range(len(slaves)):
        # Create SSH session for the slave
        slaveClients[f'slave{i}'] = utils.connect_ssh(slaves[i].public_ip_address) 
        # Configure the slave
        configure_slave(slaveClients[f'slave{i}'], master, slaves, f'slave{i}')
        # Close the SSH session
        slaveClients[f'slave{i}'].close()
        print(f"SSH session to host unbuntu@{ slaves[i].public_ip_address } closed")

    # Check status of cluster
    masterClient.run("kubectl get nodes")
    masterClient.run("kubectl get pods --all-namespaces")

    # close the client connection once the job is done
    masterClient.close()
    print(f"SSH session to host unbuntu@{ master.public_ip_address } closed") 

if __name__ == '__main__':
    print("="*88)
    print("Starting deployment of Kubernetes.")
    print("="*88)

    # Fetch and extract instances from cbdproject
    vm_instances = utils.get_running_instances()

    # Fetch private subnet of cbdproject
    subnet = utils.get_subnet()

    deploy(vm_instances, subnet['CidrBlock'])
    print("Kubernetes deployed successfully !")