"""
Purpose

Start and execute Spark for a specific application.
"""

import utils

# Parameters of Spark execution
java_application = "WordCount.java"
input_file_size = "22" #size of 2^x bits

def start_and_compute(master_client, master_pub_ip):
    '''
    Start and execute the Spark tool.
    :param master_client: The SSH client of the master.
    :param master_pub_ip: The public IP address of the master. 
    '''
    # Cleaning previous configuration
    master_client.run("cd ~/sujet-tp-scale ; source stop.sh", hide='both')

    # Starting cluster and executing with parameters
    print("Starting Hadoop and Spark...", end='', flush=True)
    master_client.run(f"cd ~/sujet-tp-scale ; source comp.sh {java_application}")
    master_client.run(f"cd ~/sujet-tp-scale ; source generate.sh filesample.txt {input_file_size}")
    master_client.run("cd ~/sujet-tp-scale ; source start.sh", hide='both')
    print("Done") 
    print(f"See the cluster overview on http://{master_pub_ip}:50070")
    master_client.run("cd ~/sujet-tp-scale ; source copy.sh")
    print("Running Spark application...", end='', flush=True)
    result = master_client.run("cd ~/sujet-tp-scale ; source run.sh", hide='both')
    print("Done")

    exec_time = "Undefined"
    # Parse the result and extract the execution time
    parsed_result = result.stdout.splitlines()
    for i in range(len(parsed_result)):
        if parsed_result[i].startswith("time in ms"):
            exec_time = parsed_result[i]
            
    print("-"*30)
    print(f"Execution {exec_time}")
    print("-"*30)

if __name__ == '__main__':
    print("="*88)
    print("Starting execution of Spark.")
    print("="*88)

    # Fetch and extract instances from cbdproject
    vm_instances = utils.get_running_instances()

    master = utils.get_master(vm_instances)
    master.ssh = utils.connect_ssh(master.public_ip_address)

    start_and_compute(master.ssh, master.public_ip_address)
    
    master.ssh.close()
    print(f"SSH session to host unbuntu@{ master.public_ip_address } closed")

    print("Spark execution finished !")